
(in-package :lambdamundo)

;; keyboard handling --------------------


(defparameter *standard-key-fns* (make-hash-table) 
  "Hash table to map keypresses to function calls")

(defparameter *console-key-fns* (make-hash-table) 
  "Hash table to map keypresses to function calls")


(defparameter *mouse-free* t
  "Indicates whether the mouse pointer is free or not")

(defmacro make-key-function (fn-table (key action-sym) &body forms)
  "Compile a body and place it in the key hashtable, to be called 
when the given key is pressed."
  `(setf (gethash ,key ,fn-table)
         (compile nil '(lambda (,action-sym)  ,@forms))))

(defmacro make-key-press-function (fn-table (key action-sym) &body forms)
  "Compile a body and place it in the key hashtable, to be called 
when the given key is pressed."
  `(setf (gethash ,key ,fn-table)
         (compile nil '(lambda (,action-sym) 
                        (when (= ,action-sym glfw:+press+) ,@forms)))))

(defun kill-key-function (fn-table key)
  "Remove a key function from the list"
  (remhash key fn-table))


