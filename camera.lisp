
(in-package :lambdamundo)

;; camera -- this could be an actor..

(defclass camera (actor)
  ((speed :initform 1.0 :accessor speed-of :type single-float)
   (zoom-speed-of :initform 1.0 :accessor zoom-speed-of :type single-float)))


(def-tuple-op translate-scaled-vertex 
    ((p vertex3d (x y z w))
     (v vector3d (vx vy vz))
     (delta single-float))
      "Move the given vertex in the direction of the given vector, uniformly scaled by delta."
      (:return vertex3d
               (vertex3d*
                (+ x (* vx delta))
                (+ y (* vy delta))
                (+ z (* vz delta))
                w)))

(defmethod pan ((cam camera) dx dy)
  "Move the camera by dx along cross vector and by dy along z vector"
  (setf (location-of cam)
        (translate-scaled-vertex 
         (location-of cam) 
         (direction-of cam)  
         (* dy (- (speed-of cam)))))
  (setf (location-of cam) 
        (translate-scaled-vertex 
         (location-of cam) 
         (cross-of cam) 
         (* dx  (speed-of cam)))))
        
(defmethod dolly ((cam camera) dx dy)
  (setf (orientation-of cam)
        (quaternion-product 
         (orientation-of cam)
         (angle-axis-quaternion 
          (angle-axis* 0.0 0.0 1.0  (* *mouse-sensitivity* dy)))))
  (setf (orientation-of cam)
        (with-vector3d 
            (cross-of cam)
            (cx cy cz)
          (quaternion-product 
           (orientation-of cam)
           (angle-axis-quaternion 
            (angle-axis* cx cy cz  (* *mouse-sensitivity* dy)))))))

(defmethod zoom ((cam camera) dz)
  (setf (location-of cam)
        (with-vertex3d
            (location-of cam)
            (px py pz pw)
          (vertex3d* px py (- pz (* (zoom-speed-of cam))) pw))))


(defparameter *modelview-debug* (make-array 16 :element-type '(single-float)))

;; (defmethod camera-modelview-matrix ((cam camera))
;;   (gl:matrix-mode gl:+modelview+)
;;   (gl:load-identity)
;;   (with-vector3d  
;;       (vector3d-sum (vertex3d-vector3d  (location-of cam)) (direction-of cam))
;;         (lx ly lz)
;;     (with-vector3d
;;         (vertex3d-vector3d (location-of cam))
;;         (px py pz)
;;       (with-vector3d 
;;           (up-of cam)        
;;           (ux uy uz)  
;;         #|        (format *debug-io* "Look at~&Position ~A ~A ~A~&LookAt ~A ~A ~AUp ~A ~A ~A" px py pz lx ly lz ux uy uz) |#
;;         (glu:look-at  
;;          px py pz
;;          lx ly lz
;;          ux uy uz)))))




(def-tuple-op gl-load-matrix44
  ((mat matrix44 (e00 e01 e02 e03
                  e10 e11 e12 e13
                  e20 e21 e22 e23
                  e30 e31 e32 e33)))
  "Load a matrix44 onto the opengl stack"
  (cffi:with-foreign-object (glmat :float 16)
      (setf (cffi:mem-aref glmat :float 0) e00)
      (setf (cffi:mem-aref glmat :float 1) e10)
      (setf (cffi:mem-aref glmat :float 2) e20)
      (setf (cffi:mem-aref glmat :float 3) e30)

      (setf (cffi:mem-aref glmat :float 4) e01)
      (setf (cffi:mem-aref glmat :float 5) e11)
      (setf (cffi:mem-aref glmat :float 6) e21)
      (setf (cffi:mem-aref glmat :float 7) e31)

      (setf (cffi:mem-aref glmat :float 8) e02)
      (setf (cffi:mem-aref glmat :float 9) e12)
      (setf (cffi:mem-aref glmat :float 10) e22)
      (setf (cffi:mem-aref glmat :float 11) e32)

      (setf (cffi:mem-aref glmat :float 12) e03)
      (setf (cffi:mem-aref glmat :float 13) e13)
      (setf (cffi:mem-aref glmat :float 14) e23)
      (setf (cffi:mem-aref glmat :float 15) e33)
      (gl::%load-matrix-f glmat)))


(defmethod camera-modelview-matrix ((cam camera))
  "Return a matrix44 equivalent to glu:look-at"
  (gl:matrix-mode gl:+modelview+)
  (gl:load-identity)
  (gl-load-matrix44
   (with-vertex3d
       (location-of cam)
       (lx ly lz lw)
     (with-vector3d  
         (direction-of cam)
         (fx fy fz)
       (with-vector3d
           (up-of cam)
           (ux uy uz)
         (with-vector3d 
             (cross-of cam)        
             (sx sy sz)  
           (matrix44* sx  sy   sz   lx
                      ux  uy   uz   ly
                      fx  fy   fz   lz
                      0.0 0.0  0.0  1.0)))))))


(defmacro with-camera (camera &body forms)
  `(progn
    (camera-modelview-matrix ,camera)
    ,@forms))

(defun make-camera () (make-actor 'camera 
                                  :location (make-vertex3d* 0.0 0.0 -5.0 1.0)
                                  :orientation (make-quaternion* 0.0 0.0 0.0 1.0)))

(defparameter *camera* nil)

(defun set-current-camera (cam) (setf *camera* (gethash cam *actors*)))

(defun reset-camera ()
  (setf *camera* (make-actor  'camera 
                              :position (make-vertex3d* 0.0 0.0 -5.0 1.0)
                              :orientation (make-quaternion* 0.0 0.0 0.0 1.0))))



;; camera change far plane function
;; (make-key-function 
;;     (char-code #\Z)
;;   (if (eql (glfw:get-key glfw:+key-lshift+) glfw:+press+)
;;       (incf *z-far* 1.0)
;;       (decf *z-far* 1.0)))

(defmethod render ((c camera))
  (camera-modelview-matrix c)
  (gl:get-floatv gl:+modelview-matrix+ *modelview-debug*))

