
(in-package :lambdamundo)

(defparameter *turtle* nil)

(defclass turtle (actor) 
  ((scale :initform (make-vector3d* 1.0 1.0 1.0) :accessor scale-of)))

(defmethod x-scale-of ((a turtle))
  (aref (the (vector single-float 3)  (scale-of a)) (the fixnum 0)))

(defmethod y-scale-of ((a turtle))
  (the single-float
    (aref (the (vector single-float 3)  (scale-of a)) (the fixnum 1))))

(defmethod z-scale-of ((a turtle))
  (the single-float
    (aref (the (vector single-float 3)  (scale-of a)) (the fixnum 2))))

(defmethod up ((a turtle) amount)
  (setf (location-of a)
        (vector3d-vertex3d 
         (vector3d-sum 
          (vertex3d-vector3d (location-of a))
          (vector3d-scale (up-of a) amount)))))

(defmethod down ((a turtle) amount)
  (up a (- amount)))

(defmethod left ((a turtle) amount)
  (setf (location-of a)
        (vector3d-vertex3d 
         (vector3d-sum 
          (vertex3d-vector3d (location-of a))
          (vector3d-scale (cross-of a) amount)))))

(defmethod right ((a turtle) amount)
  (left a (- amount)))

(defmethod forward ((a turtle) amount)
  (setf (location-of a)
        (vector3d-vertex3d 
         (vector3d-sum 
          (vertex3d-vector3d (location-of a))
          (vector3d-scale (cross-of a) amount)))))

(defmethod backward ((a turtle) amount) 
  (forward a (- amount)))
  

;; x= red, y =green, z = blue
(defmethod render ((a turtle))
;;  (break)
  (flet 
      ((render-x-axis  ()
           ;; x
           (gl:color-3f 1.0 0.0 0.0)
         (with-vertex3d 
             (location-of a)
             (x y z w)
           (gl:vertex-3f x y z))
         (with-vector3d
             (vector3d-sum
              (vertex3d-vector3d 
               (location-of a))
              (vector3d-scale 
               (cross-of a) (x-scale-of a)))
             (x y z)
           (gl:vertex-3f x y z)))

       (render-y-axis ()
         ;; y
         (gl:color-3f 0.0 1.0 0.0)
         (with-vertex3d 
             (location-of a)
             (x y z w)
           (gl:vertex-3f x y z))
         (with-vector3d
             (vector3d-sum
              (vertex3d-vector3d 
               (location-of a))
              (vector3d-scale
               (up-of a) (y-scale-of a)))
             (x y z)
           (gl:vertex-3f x y z)))


       (render-z-axis ()
         ;; z
         (gl:color-3f 0.0 0.0 1.0)
         (with-vertex3d 
             (location-of a)
             (x y z w)
           (gl:vertex-3f x y z))
         (with-vector3d
             (vector3d-sum
              (vertex3d-vector3d 
               (location-of a))
              (vector3d-scale
               (direction-of a)
               (z-scale-of a)))             
             (x y z)
           (gl:vertex-3f x y z))))
       

    (gl:with-begin gl:+lines+
      (render-x-axis)
      (render-y-axis)
      (render-z-axis))))



                           

        
