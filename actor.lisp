

(in-package :lambdamundo)

(defparameter *actors* (make-hash-table :test 'eql))

(def-tuple-class actor
    (:tuples
     ((location :type vertex3d)
      (velocity :type vector3d)
      (orientation :type quaternion)
      (w-velocity :type vector3d))
     :slots
     ((id :allocation :class :reader id-of :initform (get-universal-time))
      (dv :type single-float :intiarg :dv :initform 0.0 :accessor dv-of)
      (dw :type single-float :initarg :dw :initform 0.0 :accessor dw-of))))
     

(defmethod initialize-instance :after ((self actor) &rest args)
  (declare (ignore args))
  (setf (gethash (id-of self) *actors*) self))
           
(defun make-actor (actor-type &rest args)
  (let* ((actor
          (apply #'make-instance actor-type args))
         (result (id-of actor)))
    (incf (slot-value actor 'id))
    result))


(def-tuple-op angular-velocity
  ((vector vector3d (vx vy vz))
   (quat quaternion (qx qy qz qw)))
  "Calculate dq/dt as a quat from an angular velocity"
  (:return quaternion
           (quaternion-scale  
            (quaternion-product
             (vector3d-quaternion vector)
             quat)  0.5)))

(defmethod update-position ((a actor))
  ;; update position
  (setf (location-of a)
        (vector3d-vertex3d
         (vector3d-sum
          (vertex3d-vector3d (location-of a))
                             (velocity-of a)))))

(defmethod update-dv ((a actor))
  ;; update velocity
  (setf (velocity-of a)
         (vector3d-scale 
          (velocity-of a)
          (dv-of a))))

(defmethod update-dw ((a actor))
  ;; update angluar velocity
  (setf (w-velocity-of a)
        (vector3d-scale (w-velocity-of a) (dw-of a))))

(defmethod update-orientation ((a actor))
  ;; update orientation
  (setf (orientation-of a)
        (quaternion-unitize
         (quaternion-sum 
          (orientation-of a)          
          (angular-velocity
           (w-velocity-of a)
           (orientation-of a))))))



(defmethod update ((a actor))
  (update-dv a)
  (update-dw a)
  (update-position a)
  (update-orientation a))
              

(defmethod up-of ((a actor))
  "Return a tuple vector representing the up axis of the camera."
  (quaternion-transform-vector3d  
   (vector3d* 0.0 1.0 0.0) 
   (orientation-of a)))

(defmethod direction-of ((a actor))
  "Return a tuple vector representing the z axis of the camera."
  (quaternion-transform-vector3d 
   (vector3d* 0.0 0.0 1.0) 
   (orientation-of a)))

(defmethod cross-of ((a actor))
  "Return a tuple vector representing the x axis of the camera."
  (quaternion-transform-vector3d 
   (vector3d* 1.0 0.0 0.0) 
   (orientation-of a)))

(defun destroy-actor (actor)
  (format *debug-io* "Goodbye ~A@%" actor)
  (let ((destructee (gethash actor *actors*)))
    (destroy destructee)
    (values))
  (values))

(defmethod destroy ((a actor))
  (values))