(in-package :lambdamundo)

;; called when window is resized


(defmacro with-lambdamundo-window ((title &key
                                          (dimensions '(0 0))
                                          (colourbits '(:redbits 0 :greenbits 0 :bluebits 0 :alphabits 0))
                                          (depthbits 32)
                                          (stencilbits 0)
                                          (mode glfw:+window+))
                                   &body forms)
  "Top level form for managing our window."
  (destructuring-bind (width height)
      dimensions
    (destructuring-bind (redbits greenbits bluebits alphabits)
        colourbits
      (destructuring-bind (&key pre start main end
                                key-callback
                                resize-callback
                                char-callback
                                mouse-callback)
          forms
        `(progn
           ,(when pre
                  `(progn ,@pre))
           (glfw:with-init-window (,title ,width ,height ,redbits ,greenbits ,bluebits ,alphabits ,depthbits ,stencilbits ,mode)
             ,@start
             ,(when mouse-callback
                    `(glfw:set-mouse-button-callback (cffi:callback ,mouse-callback)))
             ,(when key-callback
                    `(glfw:set-key-callback (cffi:callback ,key-callback)))
             ,(when resize-callback
                    `(glfw:set-window-size-callback (cffi:callback ,resize-callback)))
             ,(when char-callback
                    `(glfw:set-char-callback (cffi:callback ,char-callback)))
             (iterate
               ,@main
               (glfw:swap-buffers)
               (while (eql (glfw:get-window-param glfw:+opened+) gl:+true+)))
             ,@end))))))