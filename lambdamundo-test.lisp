


(in-package :lambdamundo)

(defparameter
    *turtle*
  (make-actor 'turtle 
              :location (make-vertex3d* 1.5 1.5 0.0 1.0)
              :orientation (make-quaternion* 0.0 0.0 0.0 1.0)))
            

(defparameter *dalek-mesh* (mixamesh:make-mesh 'lodematron:md2-mesh))

(defparameter *compiled-dalek-mesh* nil) ;;  (mixamesh:mesh-compile *dalek-mesh*))

;; (one-shot-compile #P"mesh-compiler.lisp")

(defparameter *dalek* nil)

(defparameter *dalek-md2*
        (setf *dalek*
              (with-open-file 
                  (dalek-md2
                   (merge-pathnames #P"dalekx/tris.md2") 
                   :direction :input 
                   :element-type '(unsigned-byte 8))
                (lodematron:parse-md2-file dalek-md2))))

(lodematron::pose *dalek-md2* (gethash *dalek-mesh* *meshes*) "stand16" "brit")

(bound-mesh *dalek-mesh*)

(one-shot (mixamesh::make-compiled-mesh *dalek-mesh* :skin (lodematron::skin-of (gethash *dalek-mesh* *meshes*))))

(defparameter *dalek-actor* 
  (make-actor 'npc 
              :mesh *dalek-mesh*
              :location (make-vertex3d (location-of (gethash *turtle* *actors*)))
              :orientation (make-quaternion (orientation-of (gethash *turtle* *actors*)))))

(defparameter *dalek-actor* (make-npc  *dalek-mesh*))

(destroy-actor *dalek-actor*)


(up *turtle* 0.5)