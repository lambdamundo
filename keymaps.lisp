

(in-package :lambdamundo)

;; map keys to functions 

;; default quit the window function
(make-key-function *standard-key-fns*
    (glfw:+key-esc+ action)
  (when (= action glfw:+press+)
    (glfw:close-window)))

(make-key-function *standard-key-fns*
    (glfw:+key-space+ action)
    (when (= action glfw:+press+)
      (setf *mouse-free* (not *mouse-free*))
      (if *mouse-free*
          (glfw:enable glfw:+mouse-cursor+)
          (glfw:disable glfw:+mouse-cursor+))))

(make-key-press-function *standard-key-fns*
    (glfw:+key-f10+ action)
  (setf glrepl:*console* (not glrepl:*console*)))


;; console stuff

;;       ;; backspace removes previous char
(make-key-press-function *console-key-fns*
    (glfw:+key-backspace+ action)
  (glrepl:del-char-left *glwindow*))

;;       ;; del removes next char

(make-key-press-function *console-key-fns*
    (glfw:+key-del+ action)
  (glrepl:del-char-right *glwindow*))

(make-key-press-function *console-key-fns*
    (glfw:+key-f5+ action)
  (progn
    (setf (glrepl:current-result-line *glwindow*) 
          (glrepl:console-eval (glrepl:current-line-as-string glrepl:*glwindow*)))
    (glrepl:add-to-history *glwindow* (glrepl:current-line-as-string glrepl:*glwindow*))))

;; arrows move cursor
(make-key-press-function *console-key-fns*
    (glfw:+key-left+ action)
  (glrepl:cursor-left glrepl:*glwindow*))

(make-key-press-function *console-key-fns*
    (glfw:+key-right+ action)
  (cursor-right glrepl:*glwindow*))

;; up down through history
(make-key-press-function *console-key-fns*
    (glfw:+key-up+ action) 
  (setf (current-line *glwindow*) (glrepl:previous-history *glwindow*)))

(make-key-press-function *console-key-fns*
    (glfw:+key-down+ action) 
  (setf (current-line *glwindow*) (glrepl:next-history *glwindow*)))


(make-key-press-function *console-key-fns*
    (glfw:+key-f10+ action)
  (setf glrepl:*console* (not glrepl:*console*)))

(make-key-function *console-key-fns*
    (glfw:+key-esc+ action)
  (when (= action glfw:+press+)
    (glfw:close-window)))

