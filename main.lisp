
(in-package :lambdamundo)

(defparameter *one-shot-fn* nil)
(defparameter *in-main-loop* nil)
(defparameter *fps* 0.0)
(defparameter *sample-interval* 100) 
(defparameter *time-last-sample* 0)
(defparameter *frame* 0)
(defparameter *dalek-md2* nil)
(defparameter *dalek-mesh* nil)
(defparameter *dalek-actor* nil)

;; callbacks --------------------

;; window size / projection

(cffi:defcallback lambdamundo-resize-callback
    :void ((w :int) (h :int))
  (setf (win-width-of glrepl:*glwindow*) w)
  (setf (win-height-of glrepl:*glwindow*) h)
  (let* ((h/w (/ h w))
         (znear 5)
         (zfar 30)
         (xmax (* znear  0.5)))    
    (gl:viewport 0 0 w h)
    (gl:with-setup-projection 
            (glu:perspective 45.0 h/w 0.1 50.0))))

;;     (gl:with-setup-projection 
;;       (gl:frustum (- xmax) xmax (* (- xmax) h/w) (* xmax h/w) znear zfar))
    
;;     (gl:load-identity)
;;     (gl:translate-f 0 0 -20)))


;; keyboard

(cffi:defcallback lambdamundo-char-callback :void ((key :int) (action :int))
  ;; (format t "Char ~A " key)
  (when (and glrepl:*console* (= action glfw:+press+))
    (let ((c (code-char key)))
      (when c
        (glrepl:add-char glrepl:*glwindow* (code-char key))))))

(cffi:defcallback lambdamundo-key-callback :void ((key :int) (action :int))
  (unless glrepl:*console* (when-funcall (gethash key *standard-key-fns*) action))
  (when glrepl:*console*   (when-funcall (gethash key *console-key-fns*) action)))

;; mouse

(defparameter *mouse-wheel-pos* 0)
(defparameter *mouse-wheel-delta* 0)
(defparameter *mouse-wheel-changed* nil)

(defun render-debug ()
  (glrepl::render-string
   (format nil "~6,2F ~6,2F ~6,2F ~6,3F " (aref *modelview-debug* 0) (aref *modelview-debug* 1) (aref *modelview-debug* 2) (aref *modelview-debug* 3))
   0 20)
  (glrepl::render-string
   (format nil "~6,2F ~6,2F ~6,2F ~6,3F " (aref *modelview-debug* 4) (aref *modelview-debug* 5) (aref *modelview-debug* 6) (aref *modelview-debug* 7))
   0 21)
  (glrepl::render-string
   (format nil "~6,2F ~6,2F ~6,2F ~6,3F " (aref *modelview-debug* 8) (aref *modelview-debug* 9) (aref *modelview-debug* 10) (aref *modelview-debug* 11))
   0 22)
  (glrepl::render-string
   (format nil "~6,2F ~6,2F ~6,2F ~6,3F " (aref *modelview-debug* 12) (aref *modelview-debug* 13) (aref *modelview-debug* 14) (aref *modelview-debug* 14))
   0 23))


(cffi:defcallback lambdamundo-mouse-wheel-callback :void ((pos :int))
  (setf *mouse-wheel-delta* (- pos *mouse-wheel-pos*))
  (setf *mouse-wheel-pos* pos)
  (setf *mouse-wheel-changed* t))

(defun callback-set ()
  (glfw:set-key-callback (cffi:callback lambdamundo-key-callback))
  (glfw:set-char-callback (cffi:callback lambdamundo-char-callback))
  (glfw:set-mouse-wheel-callback (cffi:callback lambdamundo-mouse-wheel-callback))
;;  (glfw:set-window-size-callback (cffi:callback lambdamundo-resize-callback))

)

(defun callback-clear ()
  (glfw:set-key-callback (cffi:null-pointer))
  (glfw:set-char-callback (cffi:null-pointer))
  (glfw:set-window-size-callback (cffi:null-pointer))
  (glfw:set-mouse-wheel-callback (cffi:null-pointer)))

;; gl init and de-init --------------------

(defun begin-gl ()
  (gl:enable gl:+texture-2d+)
  (gl:enable gl:+blend+)
  (gl:enable gl:+depth-test+)
  (gl:disable gl:+cull-face+)
  (gl:disable gl:+lighting+))

(defun end-gl ())

;; drawing --------------------
;; each cell will have to know how to cull itself

(eval-when ( :load-toplevel :compile-toplevel :execute )
  (defparameter *draw-fns* (make-hash-table :test 'equalp)
   "An table of functions called in order to render the scene")

  (defparameter *draw-array* (make-array 0 :adjustable t :fill-pointer 0)
    "An array that indexes *draw-fns* to establish draw order" )

  (defun extend-draw-array (name priority)
    "If the name is in the array, adjust priority, else add it to the array"
    (assert (not (null (gethash name *draw-fns*))) (name) "~S is not a drawable")
    (let  
        ((draw-priority-pos
          (position-if #'(lambda (x) (equal (car x) name)) *draw-array*)))
      (if draw-priority-pos
          (setf (aref *draw-array* draw-priority-pos) (cons name priority))
        (vector-push-extend (cons name priority) *draw-array*)))))

(defmacro make-draw-function (name priority &body forms)
  `(progn
     (setf (gethash ,name *draw-fns*)
           (compile nil '(lambda ()  ,@forms))) 
     (extend-draw-array ,name ,priority)
     (sort *draw-array* #'(lambda (a b)
                            (< (car a) (car b))))))

(defun render-world ()
  (when (glrepl:*console*)
    (gl:disable gl:+blend+))
  (with-camera *camera*
#|
    (iterate 
      (for entry in-vector *draw-array*)
      (funcall (gethash (car entry) *draw-fns*)))
|#
    (iterate
      (for (key actor) in-hashtable *actors*)
      (render actor))))
#|
(make-draw-function 
 "testcube" 1 
 (let ((vertices
        (list 
         '(1.0 1.0 1.0)
         '(1.0 1.0 -1.0)
         '(1.0 -1.0 1.0)
         '(1.0 -1.0 -1.0)
         '(-1.0 1.0 1.0)
         '(-1.0 1.0 -1.0)
         '(-1.0 -1.0 1.0)
         '(-1.0 -1.0 -1.0)))
       (polys
        (list
         '(2 1 3 4)
         '(5 6 7 8)
         '(1 2 6 5)
         '(4 3 7 8)
         '(3 1 5 7)
         '(2 4 8 6))))
   (gl:with-begin gl:+quads+
     (gl:color-3f 0.0 1.0 0.0)
     (iterate
       (for poly in polys)
       (iterate
         (for vertex in poly)
         (apply #'gl:vertex-3f (nth (1- vertex) vertices)))))))
|#

;; animation --------------------

(defun update-world (dt)
  (when *mouse-wheel-changed*
    (pan *camera* 0.0 (* *mouse-wheel-delta* dt))
    (setf *mouse-wheel-changed* nil))
  (iterate
    (for (key actor) in-hashtable *actors*)
    (update-dv actor)))

(defmacro one-shot (&rest forms)
  `(setf *one-shot-fn* #'(lambda () ,@forms)))

(defmacro one-shot-compile (pathname)
  `(setf *one-shot-fn* #'(lambda ()
                           (multiple-value-bind 
                                 (output-file  warnings-p failure-p)  
                               (compile-file (merge-pathnames ,pathname) :verbose t :print t)
                             (declare (ignore warnings-p))
                             (when (not failure-p)
                               (load output-file :print t))))))


(defun sample-function (t0)
  (unless (zerop *frame*)
    (format *debug-io*  "Frame ~D " *frame*)
    (format *debug-io*  "Elapsed time ~D " (- t0 *time-last-sample*))
    (format *debug-io*  "Fps ~D " (/ (- t0 *time-last-sample*) *sample-interval*))
    (format *debug-io*  "Actors ~D~%" (1- (hash-table-size *actors*))))
  (setf *time-last-sample* t0)
  (make-actor 'npc 
              :mesh *dalek-mesh*
              :location (make-vertex3d* 0.0 0.0 0.0 1.0)
              :orientation (make-quaternion* 0.0 0.0 0.0 1.0)
              :dv 0.02
              :velocity
              (vector3d
               (vector3d-normal 
                (vector3d (random 1.0) 0.0 (random 1.0))))))

;; main routine -------------------
(defun main-loop ()
  (let ((t0 (coerce (glfw:get-time) 'single-float))
        (dt 0.0))
    (setf *mouse-wheel-pos* (glfw:get-mouse-wheel))
    (setf *mouse-wheel-delta* 0)
    (setf glrepl:*console-render-debug-fn* #'render-debug)
    (glfw:sleep 0.05d0)
    (gl:clear-color 0.0 0.0 0.0 1.0)
    (setf *in-main-loop* t)
    (setf *frame* 0)
    (setf glrepl:*console* nil)
    (iterate     
      (while (= (glfw::get-window-param glfw:+opened+) glfw:+true+))
      (gl:clear (logior gl:+color-buffer-bit+ gl:+depth-buffer-bit+))
      (setf dt (- (coerce (glfw:get-time) 'single-float) t0))
      (setf t0 (coerce (glfw:get-time) 'single-float))
      (when (zerop (mod *frame* *sample-interval*))
                   (sample-function t0))
      (update-world dt)
      (gl:viewport 0 0 (win-width-of glrepl:*glwindow*) (win-height-of glrepl:*glwindow*))
      (gl:matrix-mode gl:+projection+)
      (gl:load-identity)
      (glu:perspective 45.0 (/ (win-height-of glrepl:*glwindow*) (win-width-of glrepl:*glwindow*)) 0.1 50.0)
      (gl:matrix-mode gl:+modelview+)
      (render-world)
      (when glrepl:*console*
          (glrepl:render-console))
      (when *one-shot-fn*
        (funcall *one-shot-fn*)
        (setf *one-shot-fn* nil))
      ;; update
      ;; check for time available if time is avaliable render
      ;; surrender any cpu time..
      (incf *frame*)
      (glfw:swap-buffers))
    (setf *in-main-loop* nil)))

(defun begin-swank ()
  (unless *swank-port* 
    (setf *swank-port* (swank::start-session 4112))
    (format t "Please fire up your emacs and connect.~%")
    (iterate
      (while (zerop  (length swank::*connections*)))
      (cl:sleep 0.1)
      (format t ".")
      (force-output))
    (format t "~%Connected.~%")))

(defun end-swank ()
  (when (not (zerop (length swank::*connections*)))          
     (swank::end-session *swank-port*))
 (setf *swank-port* nil))

  
(defun oh-bum ()
  "Cleanup when something went wrong."
;;  (end-swank)
  (when (not *in-main-loop*)
    (clrhash *textures*)
    (destroy-font (font-of glrepl:*glwindow*))
    (iterate
     (for (key actor) in *actors*)
     (destroy actor))
    (glfw:close-window)
  (glfw:terminate)))

;; to do  -- wipe out previous state
;;   (setf *actors* )
;;   (setf *bounding-boxes* )
;;   (setf *compiled-meshes* )
;;   (setf *textures* )
;;   (setf *meshes* )

;; wipe out ogl resources
(defun glfw-cleanup ()
  (when (not *in-main-loop*)
    (glfw:close-window)
    (glfw:terminate)))



;;
(defun lambdamundo ()
  (if (glfw::init)
      (progn
        (setf glrepl:*glwindow* (make-instance 'glrepl-window))
        (add-line glrepl:*glwindow*)
        (add-line glrepl:*glwindow*)
        (add-string glrepl:*glwindow* "(one-shot-compile #P\"mesh-compiler.lisp)\"")
        (if (glfw:open-window 
             (win-width-of *glwindow*) 
             (win-height-of *glwindow*) 
             16 16 16 16 32)
            (progn
              (glfw:set-window-title "Lambdmundo")
              (progn
                (begin-gl)
                (begin-swank)
                (format t "Making font..~%")
                (setf (font-of glrepl:*glwindow*) (make-font (merge-pathnames #P"VeraMono.ttf")))
                (format t "Done.")
                (format t "Compiling mesh compiler..~%")
                (gl-ext:load-extension "ARB_vertex_program")
                (gl-ext:load-extension "ARB_vertex_buffer_object")
                (one-shot-compile #P"mesh-compiler.lisp")
                (format t "Done..~%")                
                (format t "Loading Dalek.. ~%")
                (setf *dalek-md2*
                      (with-open-file 
                          (dalek-md2
                           (merge-pathnames #P"dalekx/tris.md2") 
                           :direction :input 
                             :element-type '(unsigned-byte 8))
                        (lodematron:parse-md2-file dalek-md2)))
                (format t "Processing Dalek.. ~%")
                (setf *dalek-mesh* (mixamesh:make-mesh 'lodematron:md2-mesh))
                (lodematron::pose *dalek-md2* (gethash *dalek-mesh* *meshes*) "stand16" "brit")
                (format t "Compiling Dalek.. ~%")
                (mixamesh::make-compiled-mesh *dalek-mesh* :skin (lodematron::skin-of (gethash *dalek-mesh* *meshes*)))
                (format t "Dalek compiled into VBO.. ~%")
                (glfw:swap-interval 1)
                (glfw:enable glfw:+key-repeat+)
                (callback-set)
                (set-current-camera (make-camera))
                (main-loop)
                (callback-clear)
;;                (end-swank)
                (end-gl)
                (if (= (glfw::get-window-param glfw:+opened+) glfw:+true+)
                    (glfw:close-window))
                (glfw:terminate)))
            (progn
              (glfw:terminate)
              (error "Failed to open window"))))
        (error "Failed to init glfw")))


;; (lambdamundo-window ("Lambdamundo"
;;                     :dimensions (640 480) 
;;                     :colourbits (0 0 0 0) 
;;                     :depthbits 32 
;;                     :stencilbits 0 
;;                     :mode glfw:+window+)
;;   :key-callback 
;;   (:void ((key :int) (action :int)) )
;;   :resize-callback
;;   (:void ((width :int) (height :int))
;;          )
;;   :start
;;   ((glfw:enable glfw:+key-repeat+)
;;    (glfw:swap-interval 0)
;;    (begin-gl)
;;    (glfw:set-window-size-callback (cffi:callback window-size-callback))
;;    (glfw:set-key-callback (cffi:callback key-callback)))
;;   :main
;;   ;; to do -- we need to drop in and out of body forms
;;   ((draw)
;;    (cl:sleep 1)
;;    (animate))))

