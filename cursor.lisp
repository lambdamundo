
(in-package :lambdamundo)

(defclass cursor ()
  ((orientation :initform (new-quaternion) :accessor orientation-of)
   (location :initform (new-vertex3d) :accessor location-of)
   (attachments :initform nil))
  (:documentation "Object representing a graphical 3d location for manipulation"))

(defmethod draw ((self cursor))
  (with-vertex3d (location-of self)
      (ox oy oz ow)
    (with-matrix33
        (quaternion->matrix33 (orientation-of self))
        (xx yx zx  
         xy yy zy 
         xz yz zz)
      ;; TO DO -- nice fat lines? 
      (gl:with-begin gl:+lines+      
        (gl:color-3f 1.0 0.0 0.0)
        (gl:vertex-3f ox oy oz)
        (gl:vertex-3f xx xy xz)
        (gl:color-3f 0.0 1.0 0.0)        
        (gl:vertex-3f ox oy oz)
        (gl:vertex-3f yx yy yz)
        (gl:vertex-3f 0.0 0.0 1.0)
        (gl:vertex-3f ox oy oz)
        (gl:vertex-3f zx zy zz)))))

;; to do -- we attach things to cursors, either other cursors to create 
;; heirarchies or meshes to draw