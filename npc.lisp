

(in-package :lambdamundo)

(defclass npc (actor)
  ((mesh :initarg :mesh :accessor mesh-of)))


;; simple rendering method for debugging
(defmethod render ((self mixamesh:simple-mesh))
;;  (break)
  (iterate
    (for (values a b c) in-triangles (faces-of self))
    (with-vertex3d-aref 
        ((vertices-of self)  a  
         (ax ay az aw))
      (with-vertex3d-aref
          ((vertices-of self) b
           (bx by bz bw))
        (with-vertex3d-aref
            ((vertices-of self) c
             (cx cy cz cw))
          (gl:with-begin gl:+lines+
            (gl:vertex-3f ax ay az)
            (gl:vertex-3f bx by bz)
            (gl:vertex-3f bx by bz)
            (gl:vertex-3f cx cy cz)
            (gl:vertex-3f cx cy cz)
            (gl:vertex-3f ax ay az)))))))

(defun render-npc (a)
  (flet 
      ((render-x-axis  ()
           ;; x
           (gl:color-3f 1.0 0.0 0.0)
         (with-vertex3d 
             (location-of a)
             (x y z w)
           (gl:vertex-3f x y z))
         (with-vector3d
             (vector3d-sum
              (vertex3d-vector3d 
               (location-of a))
               (cross-of a))
             (x y z)
           (gl:vertex-3f x y z)))

       (render-y-axis ()
         ;; y
         (gl:color-3f 0.0 1.0 0.0)
         (with-vertex3d 
             (location-of a)
             (x y z w)
           (gl:vertex-3f x y z))
         (with-vector3d
             (vector3d-sum
              (vertex3d-vector3d 
               (location-of a))
               (up-of a))
             (x y z)
           (gl:vertex-3f x y z)))


       (render-z-axis ()
         ;; z
         (gl:color-3f 0.0 0.0 1.0)
         (with-vertex3d 
             (location-of a)
             (x y z w)
           (gl:vertex-3f x y z))
         (with-vector3d
             (vector3d-sum
              (vertex3d-vector3d 
               (location-of a))
               (direction-of a))             
             (x y z)
           (gl:vertex-3f x y z))))
       
       
       (gl:with-begin gl:+lines+
         (render-x-axis)
         (render-y-axis)
         (render-z-axis))))

(defun render-aabb (mesh-aabb)
  (with-aabb 
      (aabb mesh-aabb)
      (minx maxx miny maxy minz maxz)
      (gl:with-begin gl:+lines+
        (gl:color-3f 1.0 1.0 1.0)
        
        (gl:vertex-3f minx miny minz)
        (gl:vertex-3f maxx miny minz)
        
        (gl:vertex-3f maxx miny minz)
        (gl:vertex-3f maxx miny maxz)
        
        (gl:vertex-3f maxx miny maxz)
        (gl:vertex-3f minx miny maxz)
        
        (gl:vertex-3f minx miny maxz)
        (gl:vertex-3f minx miny minz)
        
        (gl:vertex-3f minx maxy minz)
        (gl:vertex-3f maxx maxy minz)
        
        (gl:vertex-3f maxx maxy minz)
        (gl:vertex-3f maxx maxy maxz)
        
        (gl:vertex-3f maxx maxy maxz)
        (gl:vertex-3f minx maxy maxz)
        
        (gl:vertex-3f minx maxy maxz)
        (gl:vertex-3f minx maxy minz)
        
        (gl:vertex-3f minx miny minz)
        (gl:vertex-3f minx maxy minz)
        
        (gl:vertex-3f maxx miny minz)
        (gl:vertex-3f maxx maxy minz)
        
        (gl:vertex-3f maxx miny maxz)
        (gl:vertex-3f maxx maxy maxz)
        
        (gl:vertex-3f minx miny maxz)
        (gl:vertex-3f minx maxy maxz))))
  
(let ((modelview-matrix (new-matrix44)))  
  (defmethod render ((n npc))
    (flet ((npc-modelview-matrix ()
             (gl:matrix-mode gl:+modelview+)
              (gl:load-matrix-f
               (setf modelview-matrix
                     (make-matrix44
                      (transpose-matrix44
                       (with-vertex3d 
                           (location-of n)
                           (x y z w)
                         (translation-matrix44 x y z))))))))                 
;;      (npc-modelview-matrix)
        (gl:matrix-mode gl:+modelview+)
        (gl:push-matrix)
        (with-vertex3d (location-of n) (x y z w)
          (gl:translate-f x y z))
        (gl:scale-f 0.01 0.01 0.01)
;;        (render-npc n)
        (render (or (gethash (mesh-of n) mixamesh:*compiled-meshes*)
                    (gethash (mesh-of n) mixamesh:*meshes*)))        
        (let ((aabb (gethash (mesh-of n) mixamesh:*bounding-boxes*)))
          (when aabb 
            (render-aabb aabb)))          
        (gl:pop-matrix))))

(defmethod make-npc (mesh)
  (let ((result 
         (make-actor 'npc 
                     :mesh mesh)))
    result))

(defmethod destroy ((n npc))
  (remhash (mesh-of n) *bounding-boxes*)
  (let ((mesh (gethash (mesh-of n) *meshes*))
        (compiled-mesh (gethash (mesh-of n) *compiled-meshes*)))
    (when mesh
      (destroy mesh)
      (remhash (mesh-of n) *meshes*))
    (when compiled-mesh
      (remhash (mesh-of n) *compiled-meshes*) 
      (destroy compiled-mesh))))