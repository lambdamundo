
(in-package :lambdamundo)

;; (unless (find-package :swank)
;;   (swank-loader:init))


(defparameter *swank-port* nil)
(defparameter *service-slime* t)

(in-package :swank)

(defun start-session (port)
  "starts a swank session and returns "
  (let* ((announce-fn #'simple-announce-function)
         (external-format (find-external-format-or-lose *coding-system*))
         (socket (create-socket *loopback-interface* port))
         (local-port (local-port socket)))
    (declare (type function announce-fn))
    (funcall announce-fn local-port)
    (format *debug-io* "Swank on port ~A " local-port)
    (initialize-multiprocessing
     (lambda ()
       (spawn (lambda ()
                (loop do
                     (ignore-errors 
                       (serve-connection socket :spawn t external-format))
                     while lambdamundo::*service-slime*))
              :name 
              (concatenate 'string "Swank " 
                                      (princ-to-string port)))))
    (setf (getf *listener-sockets* port) (list :sigio socket))
    local-port))

(defun end-session (port)
  "Stop server running on PORT. Equivalent to swank::stop-server"
  (let* ((socket-description (getf *listener-sockets* port))
         (socket (second socket-description)))
       (let ((thread-position
              (position-if 
               (lambda (x) 
                 (string-equal (second x)
                               (concatenate 'string "Swank "
                                            (princ-to-string port))))
               (list-threads))))
         (when thread-position
           (kill-nth-thread thread-position)
           (close-socket socket)
           (remf *listener-sockets* port)))))



