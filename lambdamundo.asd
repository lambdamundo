
;;;; lambdamundo.asd
(in-package :asdf)

(defsystem :lambdamundo
  :depends-on (:iterate 
                :alexandria
                :trivial-garbage
                :flexichain 
                :swank
                :cffi 
                :cl-glfw 
                :cl-glfw-glu 
                :cl-glfw-opengl 
                :cl-glfw-opengl-version_1_1 
                :cl-glfw-opengl-version_1_2 
                :cl-tuples
                :mixamesh
                :lodematron
                :glrepl)
  :serial t
  :components ((:file "package")
               (:file "utils")
               (:file "keyboard")
               (:file "keymaps")
               (:file "mouse")
               (:file "window")
               (:file "repl")               
               (:file "actor")
               (:file "turtle")
               (:file "camera")
               (:file "drawing")               
               (:file "npc")
               (:file "main")))

