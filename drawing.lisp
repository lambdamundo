(in-package :lambdamundo)

;; gl init and de-init

(defun begin-gl ()
  (gl:enable gl:+depth-test+)
  (gl:depth-func gl:+less+)
  (gl:disable gl:+cull-face+)
  (gl:disable gl:+lighting+))

(defun end-gl ())

;; drawing --------------------


(defparameter *draw-fns* (make-hash-table :test 'equalp)
  "An table of functions called in order to render the scene")

(defparameter *draw-array* (make-array 0 :adjustable t :fill-pointer 0)
  "An array that indexes *draw-fns* to establish draw order" )

(defun reset-draw-fns ()
  "Reset all drawing functions."
  (setf *draw-fns* (make-hash-table :test 'equalp))
  (setf *draw-array* (make-array 0 :adjustable t :fill-pointer 0)))

(defun extend-draw-array (name priority)
  "If the name is in the array, adjust priority, else add it to the array"
  (assert (not (null (gethash name *draw-fns*))) (name) "~S is not a drawable")
  (let  
      ((draw-priority-pos
        (position-if #'(lambda (x) (equal (car x) name)) *draw-array*)))
    (if draw-priority-pos
        (setf (aref *draw-array* draw-priority-pos) (cons name priority))
        (vector-push-extend (cons name priority) *draw-array*))))

(defmacro make-draw-function (name priority &body forms)
  "Macro to wrap a draw function body and add it to the arrays"
  `(progn
     (setf (gethash ,name *draw-fns*)
           (compile nil '(lambda () ,@forms))) 
     (extend-draw-array ,name ,priority)
     (setf *draw-array*
           (sort *draw-array* #'(lambda (a b)
                                  (< (cdr a) (cdr b)))))))

(defun remove-draw-function (name)
  (remhash name *draw-fns*))

(defun draw-world ()
  (gl:clear (logior  gl:+color-buffer-bit+ gl:+depth-buffer-bit+))
  (iterate 
    (for entry in-vector *draw-array*)
    (let ((draw-fn 
           (gethash (car entry) *draw-fns*)))
      (when draw-fn (funcall draw-fn)))))


(make-draw-function 
 "testcube" 1 
 (let ((vertices
        (list 
         '(1.0 1.0 1.0)
         '(1.0 1.0 0.0)
         '(1.0 -1.0 1.0)
         '(1.0 -1.0 0.0)
         '(-1.0 1.0 1.0)
         '(-1.0 1.0 0.0)
         '(-1.0 -1.0 1.0)
         '(-1.0 -1.0 0.0)))
       (polys
        (list
         '(2 1 3 4)
         '(5 6 7 8)
         '(1 2 6 5)
         '(4 3 7 8)
         '(3 1 5 7)
         '(2 4 8 6))))
   (with-camera *camera*
     (gl:with-begin gl:+quads+
       (gl:color-3f 0.0 1.0 0.0)
       (iterate
        (for poly in polys)
        (iterate
         (for vertex in poly)
         (apply #'gl:vertex-3f (nth (1- vertex) vertices))))))))

;; (make-draw-function
;;     "triangle" 1   
;;   (gl:with-begin gl:+triangles+
;;     (gl:color-3f 1.0 0.0 0.0) (gl:vertex-3f  1.0  0.0 -5.0)
;;     (gl:color-3f 0.0 1.0 0.0) (gl:vertex-3f -1.0  1.0 -5.0)
;;     (gl:color-3f 0.0 0.0 0.0) (gl:vertex-3f -1.0 -1.0 -5.0)))
 
;; there will be a draw function per class of object