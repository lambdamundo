
(in-package :lambdamundo)

(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))

(defmacro once-only ((&rest names) &body body)
  (let ((gensyms (loop for n in names collect (gensym))))
    `(let (,@(loop for g in gensyms collect `(,g (gensym))))
       `(let (,,@(loop for g in gensyms for n in names collect ``(,,g ,,n)))
          ,(let (,@(loop for n in names for g in gensyms collect `(,n ,g)))
             ,@body)))))

(defmacro when-funcall (x &rest parameters)
  "If the first argument is not NIL, assume it's a function and call
it with the supplied parameters."
  (with-gensyms (fun) 
    `(let ((,fun ,x))
       (when ,fun
         (funcall ,fun ,@parameters)))))
