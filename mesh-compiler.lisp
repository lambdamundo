


(in-package :mixamesh)

;; this is an extension to mixamesh, but as it involves using ogl extensions, we have to compile
;; when OGL is active in order to successfuly aquire extension function vectors and avoid massive
;; warnings and even crashes with some video drivers (ATI, *cough*, *cough*...)

;; as this uses extensions it has to be compiled and loaded with an active gl context
(defgeneric compile-mesh (mesh &rest args))

(defmethod compile-mesh ((mesh base-mesh) &rest args)
  (declare (ignorable args))
  ;; this has only faces!
  (format t "Stub called ~%")
  (values))

;; being able to map across tuple arrays would make this a lot easier.

(defmethod compile-mesh ((self simple-mesh) &rest args)
  "Given a mesh return a compiled mesh, which is a non-modifiable mesh optimised for rendering in foreign memory."
  (declare (ignorable args))
  (format t "Compiling simple mesh ~%")
  (flet ((make-buffer ()
           "Create a buffer-name for an OpenGL vertex buffer object"
           (let ((buffer-name (make-array 1 :element-type '(unsigned-byte 32))))
             (gl:gen-buffers-arb 1 buffer-name)
             (aref buffer-name 0))))
    (let ((result (make-instance 'compiled-mesh))
          (triangle-data (cffi::foreign-alloc :uint32 :count (* 3 (triangle-array-dimensions (faces-of self)))))
          (vertex-data (cffi:foreign-alloc :float :count (* 3 (vertex3d-array-dimensions (vertices-of self)))))
          (vertex-buffer (make-buffer))
          (triangle-buffer (make-buffer)))
      (flet ((copy-triangle-buffer ()
               "Copy mesh triangle data into vertex buffer"
               (iterate
                 (with face-index = 0)
                 (for (values a b c) in-triangles (faces-of self))
                 (setf (cffi:mem-aref triangle-data :uint32 face-index) a)
                 (incf face-index)
                 (setf (cffi:mem-aref triangle-data :uint32 face-index) b)
                 (incf face-index)
                 (setf (cffi:mem-aref triangle-data :uint32 face-index) c)
                 (incf face-index))
               (gl:bind-buffer-arb gl:+element-array-buffer-arb+ triangle-buffer)
               (gl:buffer-data-arb gl:+element-array-buffer-arb+
                                   (* 3 (triangle-array-dimensions (faces-of self)) (cffi:foreign-type-size :uint32))
                                   triangle-data
                                   gl:+static-draw-arb+))
             (copy-vertex-buffer ()
               "Copy mesh vertex data into vertex buffer"
               (iterate
                 (with vertex-index = 0)
                 (for (values x y z w) in-vertices (vertices-of self))
                 (setf (cffi:mem-aref vertex-data :float vertex-index) x)
                 (incf vertex-index)
                 (setf (cffi:mem-aref vertex-data :float vertex-index) y)
                 (incf vertex-index)
                 (setf (cffi:mem-aref vertex-data :float vertex-index) z)
                 (incf vertex-index))
               (gl:bind-buffer-arb gl:+array-buffer-arb+ vertex-buffer)
               (gl:buffer-data-arb gl:+array-buffer-arb+
                                   (* 3 (vertex3d-array-dimensions (vertices-of self)) (cffi:foreign-type-size :float))
                                   vertex-data
                                   gl:+static-draw-arb+)))
        (copy-vertex-buffer)
        (setf (slot-value result 'vertex-buffer) vertex-buffer)
        (copy-triangle-buffer)
        (setf (slot-value result 'triangle-buffer) triangle-buffer)
        (setf (slot-value result 'element-count) (* 3 (triangle-array-dimensions (faces-of self))))
        (cffi:foreign-free vertex-data)
        (cffi:foreign-free triangle-data))
      result)))

(defun make-texture-from-skin (skin)
  (let* ((texture (gethash skin *textures*))
         (result (make-instance 'glrepl:rgba-image :width (width-of texture) :height (height-of texture))))
    (format *debug-io* "Map size ~A~%" (colour-array-dimensions (map-of texture)))
    (format *debug-io* "texture size ~A~%" (* (width-of texture) (height-of texture)))
    (iterate
      (for index from 0 below (* (width-of texture) (height-of texture)))
      (cl-tuples:with-colour-aref 
          ((map-of texture)  
           index
           (r g b a))
        (setf (glrepl:pixel result index)
              (glrepl:pixval (coerce (floor (* 255 r)) '(unsigned-byte 8)) 
              (coerce (floor (* 255 g)) '(unsigned-byte 8))
              (coerce (floor (* 255 b)) '(unsigned-byte 8))
              255))))
    (glrepl:update-image result)
    result))


(defmethod compile-mesh ((self lodematron:md2-mesh) &rest args)
  (destructuring-bind
        (&key skin)
      args
    (format t "Compiling md2 mesh with ~D tris and ~D uv tris ~%" (length (faces-of self)) (length (lodematron:uvs-tris-of self)))
    (flet ((make-buffer ()
             "Create a buffer-name for an OpenGL vertex buffer object"
             (let ((buffer-name (make-array 1 :element-type '(unsigned-byte 32))))
               (gl:gen-buffers-arb 1 buffer-name)
               (aref buffer-name 0))))
      (let* ((result (make-instance 'mixamesh:textured-compiled-mesh))
             (uv-data (cffi::foreign-alloc :float :count (* 2 3 (triangle-array-dimensions (lodematron:uvs-tris-of self)))))
             (vertex-data (cffi:foreign-alloc :float :count (* 3 3 (triangle-array-dimensions (faces-of self)))))
             (vertex-buffer (make-buffer))
             (uv-buffer (make-buffer)))

        (flet ((copy-uv-buffer ()
                 "Copy uv triangle data into vertex buffer"
                 (iterate
                   (with uv-index = 0)
                   (for (values a b c) in-triangles (lodematron:uvs-tris-of self))

                   (with-vector2d-aref
                       ((uvs-of self)
                        a
                        (u v))

                     (setf (cffi:mem-aref uv-data :float uv-index) u)
                     (incf uv-index)
                     (setf (cffi:mem-aref uv-data :float uv-index) v)
                     (incf uv-index))

                   (with-vector2d-aref
                       ((uvs-of self)
                        b
                        (u v))

                     (setf (cffi:mem-aref uv-data :float uv-index) u)
                     (incf uv-index)
                     (setf (cffi:mem-aref uv-data :float uv-index) v)
                     (incf uv-index))

                   (with-vector2d-aref
                       ((uvs-of self)
                        c
                        (u v))

                     (setf (cffi:mem-aref uv-data :float uv-index) u)
                     (incf uv-index)
                     (setf (cffi:mem-aref uv-data :float uv-index) v)
                     (incf uv-index)))

                 (gl:bind-buffer-arb gl:+array-buffer-arb+ uv-buffer)
                 (gl:buffer-data-arb gl:+array-buffer-arb+
                                     (* 2 3 (triangle-array-dimensions (lodematron:uvs-tris-of self)) (cffi:foreign-type-size :float))
                                     uv-data
                                     gl:+static-draw-arb+))

               (copy-vertex-buffer ()
                 "Copy mesh vertex data into vertex buffer"
                 (iterate
                   (with vertex-index = 0)
                   (for (values a b c) in-triangles (faces-of self))
                     
                   (with-vertex3d-aref
                       ((vertices-of self)
                        a
                        (x y z w))

                       
                     (setf (cffi:mem-aref vertex-data :float vertex-index) x)
                     (incf vertex-index)
                     (setf (cffi:mem-aref vertex-data :float vertex-index) y)
                     (incf vertex-index)
                     (setf (cffi:mem-aref vertex-data :float vertex-index) z)
                     (incf vertex-index))
                     
                   (with-vertex3d-aref
                       ((vertices-of self)
                        b
                        (x y z w))

                     (setf (cffi:mem-aref vertex-data :float vertex-index) x)
                     (incf vertex-index)
                     (setf (cffi:mem-aref vertex-data :float vertex-index) y)
                     (incf vertex-index)
                     (setf (cffi:mem-aref vertex-data :float vertex-index) z)
                     (incf vertex-index))
                     
                   (with-vertex3d-aref
                       ((vertices-of self)
                        c
                        (x y z w))

                     (setf (cffi:mem-aref vertex-data :float vertex-index) x)
                     (incf vertex-index)
                     (setf (cffi:mem-aref vertex-data :float vertex-index) y)
                     (incf vertex-index)
                     (setf (cffi:mem-aref vertex-data :float vertex-index) z)
                     (incf vertex-index)))
                 
                 (gl:bind-buffer-arb gl:+array-buffer-arb+ vertex-buffer)
                 (gl:buffer-data-arb gl:+array-buffer-arb+
                                     (* 3 3 (triangle-array-dimensions (faces-of self)) (cffi:foreign-type-size :float))
                                     vertex-data
                                     gl:+static-draw-arb+)))
          (copy-vertex-buffer)
          (setf (slot-value result 'vertex-buffer) vertex-buffer)
          (copy-uv-buffer)
          (setf (slot-value result 'uv-buffer) uv-buffer)
          (setf (slot-value result 'element-count) (* 3 (triangle-array-dimensions (faces-of self))))
          (cffi:foreign-free vertex-data)
          (cffi:foreign-free uv-data)          
          (setf (texture-of result) (make-texture-from-skin skin)))            
        result))))
    

(defun make-compiled-mesh (mesh &key skin)
  (setf (gethash mesh *compiled-meshes*) (compile-mesh (gethash mesh *meshes*) :skin skin)))


(in-package :lambdamundo)

;;(declaim (optimize (compilation-speed 0) (debug 0) (safety 0) (space 0) (speed 3)))

(defmethod render ((self compiled-mesh))

  (gl:enable-client-state gl:+vertex-array+)
  (gl:enable-client-state gl:+index-array+)
  (gl:bind-buffer-arb  gl:+array-buffer-arb+ (vertex-buffer-of self))
  (gl:bind-buffer-arb  gl:+element-array-buffer-arb+ (triangle-buffer-of self))
  (gl:vertex-pointer 3 gl:+float+ 0 (cffi::null-pointer))
  (gl:draw-elements gl:+triangles+  (element-count-of self) gl:+unsigned-int+ (cffi:null-pointer))
  (gl:disable-client-state gl:+vertex-array+)
  (gl:disable-client-state gl:+index-array+))

(defmethod render ((self textured-compiled-mesh))
  ;;  (format *debug-io* "Redering ..~%")
  (gl:disable gl:+blend+)
  (bind (mixamesh:texture-of self) gl:+replace+)  
  (gl:color-3f 1.0 1.0 1.0)
  (gl:enable-client-state gl:+vertex-array+)
  (gl:enable-client-state gl:+texture-coord-array+)
  (gl:bind-buffer-arb  gl:+array-buffer-arb+ (vertex-buffer-of self))
  (gl:bind-buffer-arb  gl:+array-buffer-arb+ (uv-buffer-of self))
  (gl:vertex-pointer 3 gl:+float+ 0 (cffi::null-pointer))
  (gl:tex-coord-pointer 2 gl:+float+ 0 (cffi:null-pointer))
  (gl:draw-arrays gl:+triangles+  0 (element-count-of self))
  (gl:disable-client-state gl:+texture-coord-array+)
  (gl:disable-client-state gl:+vertex-array+)
  (gl:disable gl:+blend+))

;;  (format *debug-io* "Redering ..~%")
;;   (glrepl::with-opengl
;;    (gl:disable gl:+blend+)
;;    (gl:enable gl:+texture-2d+)
;;    (gl:bind-texture gl:+texture-2d+ (cffi::mem-ref (name-of (mixamesh:texture-of self)) :uint32))
;;    (gl:tex-env-f gl:+texture-env+ gl:+texture-env-mode+ gl:+decal+)
;;     (gl:color-3f 1.0 1.0 1.0)
;;     (gl:enable-client-state gl:+vertex-array+)

;;     (gl:bind-buffer-arb  gl:+array-buffer-arb+ (vertex-buffer-of self))
;;    (gl:bind-buffer-arb  gl:+array-buffer-arb+ (uv-buffer-of self))
;;     (gl:vertex-pointer 3 gl:+float+ 0 (cffi::null-pointer))

;;     (gl:draw-arrays gl:+triangles+  (element-count-of self) 0)
;;     (gl:disable-client-state gl:+vertex-array+)
;;    (gl:disable-client-state gl:+texture-coord-array+)
;;    (gl:enable gl:+blend+))
;; ))
  
(defmethod destroy ((self compiled-mesh))
  (flet ((delete-buffer (buf)
           (let ((buf-array (make-array 1)))
             (setf (aref buf-array 0) buf)         
             (gl:delete-buffers-arb 1 buf-array))))
    (when (vertex-buffer-of self)
      (delete-buffer (vertex-buffer-of self))
      (setf (slot-value 'vertex-buffer self) nil))
    (setf (slot-value 'vertex-buffer self) nil)
    (when (triangle-buffer-of self)
      (delete-buffer (triangle-buffer-of self))
      (setf (slot-value 'triangle-buffer self) nil))))
                   

(defmethod destroy ((self textured-compiled-mesh))
  (flet ((delete-buffer (buf)
           (let ((buf-array (make-array 1)))
             (setf (aref buf-array 0) buf)         
             (gl:delete-buffers-arb 1 buf-array))))
    (when (texture-of self)
      (destroy-image (texture-of self))
      (setf (texture-of self) nil))
    (when (vertex-buffer-of self)
      (delete-buffer (vertex-buffer-of self))
      (setf (slot-value 'vertex-buffer self) nil))))
                     
;;  (delete-buffer (uv-buffer-of self)
;;                   )))                  
